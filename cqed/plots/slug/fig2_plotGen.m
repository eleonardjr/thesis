load('fig2data_v5.mat')
S21_clims = [-20 20];
S21_step = 10;
S21_ticks = S21_clims(1):S21_step:S21_clims(2);
S12_clims = [-40 0];
S12_step = 10;
S12_ticks = S12_clims(1):S12_step:S12_clims(2);
SM_xlims = [-0.26 -0.08]; 
S_freq_lims = [5.2 6.2];
%S12_lims = S21_lims;
colors='jet';
S_margins=[0.04,0.025];
line_margins = [0.04 0.15];

% S21 theory
h=figure;
subplot_tight(2,3,1,S_margins)
imagesc(bias,freq,s21)
set(gca,'YDir','normal')
ylim(S_freq_lims)
h.Children.XTickLabel=[];
caxis(S21_clims)
colormap(colors)

% S21 meas
subplot_tight(2,3,2,S_margins)
imagesc(bias_m,freq_m,s21m)
set(gca,'YDir','normal')
ylim(S_freq_lims)
xlim(SM_xlims)
h.Children(1).XTickLabel=[];
h.Children(1).YTickLabel=[];
caxis(S21_clims)
colormap(colors)
hcb=colorbar;
hcb.YTick=S21_ticks;

% S12 theory
subplot_tight(2,3,4,S_margins)
imagesc(bias,freq,s12)
set(gca,'YDir','normal')
ylim(S_freq_lims)
caxis(S12_clims)
colormap(colors)

% S12 meas
subplot_tight(2,3,5,S_margins)
imagesc(bias_m,freq_m,s12m)
set(gca,'YDir','normal')
ylim(S_freq_lims)
xlim(SM_xlims)
h.Children(1).YTickLabel=[];
caxis(S12_clims)
colormap(colors)
hcb=colorbar;
hcb.YTick=S12_ticks;

% Line cuts
subplot_tight(2,3,[3 6],line_margins)
plot(line_f,line_s21,'b--','LineWidth',3,'DisplayName','S21');
hold on
plot(line_f,line_s12,'r','LineWidth',3,'DisplayName','S12');
xlim([5.5 5.9])
ylim([-45 15])
legend('show')