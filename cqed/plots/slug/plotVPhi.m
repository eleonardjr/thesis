fileList = {'blackbox_prelim_VPhi_iB1.4Lsh4.mat',...
    'blackbox_prelim_VPhi_iB1.6Lsh4.mat',...
    'blackbox_prelim_VPhi_iB1.8Lsh4.mat',...
    'blackbox_prelim_VPhi_iB1.9Lsh4.mat',...
    'blackbox_prelim_VPhi_iB1.95Lsh4.mat',...
    'blackbox_prelim_VPhi_iB2Lsh4.mat'};

figure; hold on;
for k=1:numel(fileList)
   load(fileList{k})
   plot(phiB,v_m,'DisplayName',[fileList{k}])
end
axis tight
xlim([-0.44 0.56])
grid off
set(gca,'FontSize',11)
legend('show','Location','best')

figure; hold on;
load('05-Apr-2017_bb1_SLUG_v1_10pH1Ohm6GHz21uA2squm.mat');
imagesc(phiB,f/1e9,real(G));
caxis([0 15])
colormap jet
colorbar
axis tight
ylim([5.6 6.3])
yyaxis right
plot(phiB, V_m(1,:),'w--')
set(gca,'FontSize',12)