sb = 2*pi;
t=-6*pi:0.1:6*pi;
Omega = @(t) exp(-t.^2/70);
gamma = pi/2;
sig = @(t) Omega(t) .* cos(sb.*t-gamma);

figure; hold on;
%% plot enveloped signals
%plot(t,sig(t),'k','LineWidth',0.5)
%plot(t,Omega(t),'r','LineWidth',0.5)
%plot(t,-Omega(t),'r','LineWidth',0.5)

kappa = 0.3;

Lorent = @(f,f0) 0.5 .* kappa ./ ((f-f0).^2 + (0.5*kappa)^2);
f=-1.2:0.001:1.2;

%% plot resonator dips
%plot(f,-Lorent(f,-0.5))
%plot(f,-Lorent(f,0.5))

% blobs
blob = @(sig) [normrnd(0,sig) normrnd(0,sig)];
npts = 1000;
sigma = 0.1;
blobs = zeros(npts,2);
for n=1:npts
    blobs(n,:)=blob(sigma);
end
scatter(blobs(:,1),blobs(:,2),2,'filled')
for n=1:npts
    blobs(n,:)=blob(sigma);
end
scatter(blobs(:,1),blobs(:,2)-0.707,2,'filled')
axis equal
axis off
