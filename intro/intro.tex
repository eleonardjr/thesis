\chapter{Introduction}

%\begin{wbepi}{Richard Feynman (1982)~\cite{Feynman:1982aa}}
%...I'm not happy with all the analyses that go with just the classical theory, because Nature isn't classical, dammit, and if %you want to make a simulation of Nature, you'd better make it quantum mechanical, and by golly it's a wonderful problem, because it doesn't look so easy.
%\end{wbepi}

Computing technology has been the benefactor of significant advances since the first reports of programmable electronic digital computers in the late 1940s into the early 1950s~\cite{Beautiful}. Indeed, one of the calculations used as a benchmark by Williams in 1948~\cite{WILLIAMS:1948aa,Calculating} was simply the long division of (2$^{30}$-1)/31; their report indicates that their machine took about 1.5 seconds to complete this calculation! 
At around the same time as this, and spurred by the need for such a device, the transistor was devised to replace then state-of-the-art vacuum tubes as supports of computational information~\cite{Bardeen:1948aa}; it is this invention which has spurred the now global effort to squeeze every last ounce of power from classical computational resources. 
Since then, transistors have been miniaturized to be impossibly small (reported many-atom transistors have reached the 1~nm length scale~\cite{Desai:2016aa}) and classical computational power has increased at an exponential rate, faithfully following the predictions of Moore~\cite{Moore:1965aa} for many decades.

The enormous success of classical computing has enabled the calculation and simulation of a great many physical systems, both classical and quantum mechanical; in crude terms, if one is able to write a Hamiltonian which is faithful to the physical system, it in principle can be simulated by a classical machine. This has been done for several small quantum systems. However, simulating a \emph{large} quantum mechanical system on a classical computer can be enormously difficult even for a modestly-sized machine due to the exponential memory requirements -- where a classical system of $N$ objects can typically be described by $\mathcal{O}(N)$ bits of information, an entangled quantum mechanical system requires an exponentially harder $\mathcal{O}(2^N)$ bits. This begs the question: if it is quantum mechanics which we are set to simulate, why not use a quantum mechanical system to do so? Of course, this is hard; real-world quantum systems are open to the world which means they can couple to sources of unwanted influence or dissipation (decoherence). In controlled quantum systems which exist today, a great deal of care has gone into the engineering of the unavoidable qubit-environment coupling while balancing the necessary qubit-control/measurement coupling required to build an effective quantum machine. So far, there has been a great deal of success; in the last two decades or so, progress has been fast and vast in the construction of small-scale quantum bit (qubit) arrays. While the motivator for all of this research could be inferred from the above -- the plain desire to efficiently simulate a quantum mechanical system~\cite{Feynman:1982aa,Feynman:1986aa} -- there are also many concrete examples of other tangible uses for such a quantum computer. These include, for example, the prime factorization of large composite numbers~\cite{Shor:1997aa} with applications to cryptography~\cite{Rivest:1978aa}, efficient database searching~\cite{Grover:1996aa}, and the efficient simulation of molecular spectra~\cite{Lanyon:2010aa,OMalley:2016aa,Colless:2018aa} with applications in quantum chemistry.

In this thesis, we will largely concentrate on a novel form of control of an open quantum system - the already established superconducting transmon qubit. The details of this will be covered later, and many of the concerns of many-qubit applications and what one will actually do once a large-scale quantum processor is constructed will be left to others\footnote{Indeed, an in-depth discussion and great detail into the applications of a many-qubit system are outside the scope of this thesis. The inspired reader may be interested in a more rigorous theoretical treatment as available in, e.g.,~\cite{NielsenChuang}.}. What follows in this Chapter is a brief introduction to qubits in Sec.~\ref{sec:qubits} including a non-exhaustive overview of some physical qubit implementations being pursued in modern day research and, in Sec~\ref{sec:thesisover}, a short overview for the rest of this thesis.

\section{Qubits}\label{sec:qubits}

\subsection{Brief Introduction}\label{sec:qbint}

\subsubsection{Maths}

A quantum computation comprises a series of quantum logic gates which operate on an array of many quantum bits (qubits). A qubit is a quantum mechanical system which can be defined by a wavefunction
\begin{align}
\ket{\psi}&=\alpha\ket{0}+\beta\ket{1},\\
1&=|\alpha |^2+|\beta |^2,
\end{align}
where $\alpha,\beta$ are complex probability amplitudes for each of their respective states and their squares sum to unity. From here, we see that a qubit may exist in a superposition of $\ket{0}$ and $\ket{1}$ instead of the classical bit which is only a binary 0 or 1. This can be represented on the Bloch sphere with
\begin{align}
\alpha&=\cos\frac{\theta}{2}\\
\beta&=e^{i\phi}\sin\frac{\theta}{2},
\end{align}
using polar angle $\theta$ and azimuthal angle $\phi$ [depicted in Fig.~\ref{fig:bloch}]. In matrix form, this is commonly written
\begin{align}
\ket{\psi}&=\alpha\left[\begin{matrix}1\\0\end{matrix}\right] + \beta\begin{bmatrix}0\\1\end{bmatrix}.
\end{align}
A quantum system which can be described by this wavefunction can be used as a qubit. While this is a necessary element, there are additional constraints on what is required for a technology to be used in a successful quantum processor~\cite{DiVincenzo:2000aa,Knill:2005aa,Ladd:2010aa}. It is in this definition in which a small part of the power of using a qubit to perform computation lies; a single qubit state is here described by two quantities while the state of a classical bit is described by a binary option. This scales to $N$ bits of information stored in an $N$-device classical system and an exponentially larger $2^N$ bits of classical information stored in an array of $N$ qubits. There have been many physical implementations of real physical systems which can be represented (either exactly or in a good approximation) as a quantum two-level system, some of which are briefly described in the next Section. 

\begin{figure}[t]
\includegraphics[width=\textwidth]{intro/bloch.pdf}
\caption[Bloch sphere representation of $\ket{\psi}$]{\label{fig:bloch}Comparison of possible states for a classical versus a quantum bit. \textbf{(a)}~A classical bit can only be in one of two possible positions: 0 or 1. \textbf{(b)}~The state of a qubit can be written as a superposition of $\ket{0}$ and $\ket{1}$ and can be fully determined by two quantities: the polar angle $\theta$ and azimuthal angle $\phi$~\cite{NielsenChuang}.}
\end{figure}

Quantum gates act on $\ket{\psi}$ as unitary operators~\cite{Monroe:1995aa}, satisfying, for a gate $\hat{U}$, the relation $\hat{U}^{\dagger}\hat{U}=\hat{I}$. An example is the \textsc{not} gate, $\hat{\sigma}_x$, written 
\begin{align}
\hat{\sigma}_x&=\begin{bmatrix}
0 & 1 \\
1 & 0
\end{bmatrix},\\
\hat{\sigma}_x\begin{bmatrix}\alpha\\\beta\end{bmatrix}&=\begin{bmatrix}\beta\\\alpha\end{bmatrix}.
\end{align}
This is one of the Pauli matrices, $\hat{\sigma}_a$, all of which represent $\pi$-rotations about an axis $a$ and can be written
\begin{align}
\hat{\sigma}_a&=\begin{bmatrix}
\delta_{az} & \delta_{ax}-i\delta_{ay} \\ \delta_{ax}+i\delta_{ay} & -\delta_{az}
\end{bmatrix},
\end{align} 
where $\delta_{ij}$ is the Kronecker Delta. In the later parts of this thesis, we will describe how we use our novel control scheme to achieve both $\hat{\sigma}_x$ and $\hat{\sigma}_y$ gates to control a superconducting transmon qubit.

\subsubsection{Coherence}

For a perfect qubit, the system will be prepared in some initial state and it will always be in that state when measured some time later. However, in order to prepare and measure a real qubit, there must be some coupling of the qubit to the control and measurement system. There also exists no such object as a perfect qubit since it must live in the physical world and thus have some interaction with the environment around it (such as local fluctuations in an electromagnetic field, nearby impurities, or other fluctuators which the experimentalist has tried so dashingly to minimize. These coupling provide channels of decoherence, and thus present one of the largest challenges in the construction of a many-qubit device. It is for this reason that a variety of error-protection schemes have been devised for a one-day large-scale processor~\cite{Calderbank:1996aa,Steane:1996aa,Steane:1999aa} with extensions to a geometry which is readily achievable using planar superconducting qubit circuits~\cite{Fowler:2012aa}, the hardware of choice for this thesis.

\subsection{Qubit Hardware}

There have been several demonstrated physical systems which have acted as qubits and which fulfill the numerous requirements for a technology to theoretically function as the same~\cite{DiVincenzo:2000aa,Knill:2005aa,Ladd:2010aa}. Here, we will provide a nonexhaustive outline of some of these systems and briefly comment on their characteristics.


\subsubsection{Photons}

It is difficult to imagine a more physically relevant system for quantum information processing than the photon, making it an attractive topic of intense research. The quantum states of a photon are encoded by several physical mechanisms including the spectrum, shape, arrival time, wave vector, and instantaneous position, all of which must be exquisitely controlled to limit the available Hilbert space to a two-level system~\cite{Monroe:2002aa,Walmsley:2005aa,Kok:2007aa}. One of the primary difficulties of using photons as qubits is the reliable creation of single-photons with such well-engineered parameters~\cite{Milburn:2009aa}. This has spurred many technological advances including ``ultra-bright'' sources for polarized photons pairs~\cite{Kwiat:1995aa,Kwiat:1999aa} and in modern times other schemes have been introduced which simultaneously use three degrees of freedom in photons to support qubit-like states, including up to 18-qubit entanglement using just six photons~\cite{Wang:2018aa}. 

\subsubsection{Neutral Atoms}

There have been several qubit realizations using the natural energy level splittings within the electronic structure of atoms, with notable progress in using using some of the heavy alkalis like Rb and Cs~\cite{Saffman:2010aa,Saffman:2016aa,Auger:2017aa}. Here, the two-level system of interest is defined by Nature; for identical atoms, the transitions are identical in splitting (the qubit frequency of one atom will be the same for its neighbor, and so on). This has been hailed as a great boon to neutral atom (and, similarly, trapped-ion) qubits since it significantly simplifies the manufacturing of a qubit array control system as compared to systems of synthetic atoms (read below: Quantum Dots and Josephson Circuits) regardless of the question of being able to individually address qubits without addressing their neighbors~\cite{Brown:2016aa}. There have been demonstrations of implementing quantum gates and benchmarking them in a $7\times7$ 2D lattice of single Cs atom qubits with high-fidelity~\cite{Maller:2015aa,Xia:2015aa}.

\subsubsection{Quantum Dots}

One of the signature physical objects which by itself exhibits quantum mechanical behavior is the electron; most of the complexity aside, the field of quantum dots for quantum information processing relies on using the spin of either a single electron or even a single nucleus as a qubit~\cite{Loss:1998aa,Zwanenburg:2013aa}. With the size of the physical implementations being on the atomic scale, there is an enormous modern effort to build solid state qubits out of these devices with the goal being to scale them in a fashion quite similar to the transistor and Moore's law~\cite{Moore:1965aa} in classical computing. A great deal of progress has been made in high-fidelity gate operations in silicon based qubit systems~\cite{Kawakami:2014aa,Muhonen:2014aa,Veldhorst:2014aa}, and there are many more architectures out there based in silicon which each has advantages and disadvantages and have been the subject of intense research in the past two decades~\cite{Zwanenburg:2013aa}.

\subsubsection{Josephson Circuits}

While the previously described implementations of qubits have relied on systems which are inherently quantum mechanical insofar as what physics must be used to fully describe their interactions, the field of Josephson quantum computing relies on micro- and nano-fabricated circuits comprising millions of superconducting atoms which together can be engineered to act as single ``artificial atoms~\cite{Clarke:2008aa,Devoret:2013aa,Gambetta:2017aa,Liu:2018aa}.'' These, in turn, can be individually addressed and measured using relatively straightforward methods which are mostly derived from their atomic counterparts. As will be outlined in much greater detail in Sec.\ref{sec:qdev}, Josephson computing has made rapid progress in the past two decades, and there are now schemes to come up with a test that will show that near-term devices can indeed perform certain calculations which are intractable for typicall classical simulators~\cite{Neill:2018aa}. It is within this hardware paradigm in which the work in this thesis is firmly situated. 





\section{Thesis Overview}\label{sec:thesisover}

The remainder of this thesis is organized as follows:
Chapter~\ref{chap:sce} provides an outline for a choice selection of superconducting devices and their properties as they are relevant to this work. This will include introducing the Josephson junction as a powerful nonlinear inductor and how it fits into the scope of both quantum and classical logic superconducting computing devices.
Then, in Chapter~\ref{chap:cqed}, we will introduce circuit quantum electrodynamics (cQED), a powerful toolbox used to design, measure, and analyze superconducting quantum circuits. One necessary element of an effective measurement chain when it comes to superconducting qubits is the (at least near-) quantum-limited amplifier. One such flavor of this amplifier is the superconducting low-inductance undulatory galvanometer (SLUG) amplifier, and some of its useful properties as an amplifier in the context of qubit measurement will be described therein as well.
Later, in Chapter~\ref{chap:sfq}, a hybrid device will be introduced in which a superconducting qubit is cofabricated on the same chip as a superconducting logic circuit for the purposes of coherent quantum control. The considerations and challenges related to such an integration are discussed and ultimately the performance of the control circuit is fully characterized.
Finally, in Chapter~\ref{chap:conc}, we conclude by summarizing this thesis and providing a brief outlook of some reasonable next steps for the integration of quantum and classical superconducting devices.