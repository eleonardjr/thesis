freq = 5e9;
f21 = 4.75e9;

%n_pulses = [2 3 4 5 10 25 50 100];
n_pulses = 1:12;
tstep = 0.1e-12;
time = (0:tstep:100*max(max(n_pulses)) / freq)';


NFFT = 2^nextpow2(length(time));
freqs = 1/tstep/2*linspace(0,1,NFFT/2+1);
[ ~, f21_idx] = min(abs(freqs - f21));

f21_amp = zeros(length(n_pulses),1);
norm_amp = zeros(length(n_pulses),length(freqs));
tic;
parfor n=1:length(n_pulses)
   [train] = pulse_train(freq, n_pulses(n), time);
   
   y = (fft(train,NFFT));
   norm_amp(n,:) = mat2gray(2*abs(y(1:NFFT/2+1)));
   
   %plot(freqs/1e9,norm_amp)
   
   %f21_amp(n) = norm_amp(n,f21_idx);
end
toc

figure; hold on;
for n = 1:length(n_pulses)
    plot(freqs/1e9, norm_amp(n,:),...
        'DisplayName',['Number of Pulses: ',num2str(n_pulses(n))])
    f21_amp(n) = norm_amp(n,f21_idx);
end
xlim([0 4*freq/2/1e9])
xlabel('Frequency (GHz)')
ylabel('Normalized Fourier Magnitude')

figure; hold on;
plot(n_pulses, f21_amp)