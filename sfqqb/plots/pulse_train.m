function [delta_train] = pulse_train(freq, n_pulses, time)

%time = (0:0.01e-12:n_pulses/freq)';

delta_train = zeros(length(time),1);
for n=1:n_pulses
    delta_train = delta_train + dirac(time-(n-1)/freq);
end
%delta_train = sin(2*pi*freq*time);
idx = delta_train == Inf;
delta_train(idx) = 1;

end