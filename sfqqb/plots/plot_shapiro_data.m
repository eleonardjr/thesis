d=loadMeasurementData('IV_004.txt');

numexpts = length(d.Voltage(:,1));

idx = d.SFQ_DC_Bias_Current <= 80;
fits = zeros(numexpts,2);
figure; hold on;
for n = 1:numexpts
    [xC, yC] = prepareCurveData(d.SFQ_DC_Bias_Current(idx), d.Voltage(n, idx));
    fits(n,:) = polyfit(xC,yC,1);
    
    plot(d.SFQ_DC_Bias_Current,...
        (d.Voltage(n,:)-fits(n,1).*d.SFQ_DC_Bias_Current'-fits(n,2))/1e-6,...
        'DisplayName',['Trigger Power: ', num2str(d.SFQ_Power(n)-50),' dBm'])
end

legend('show','Location','best')
xlim([40 133])
ylim([-1 15])


