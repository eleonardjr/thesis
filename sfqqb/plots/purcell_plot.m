R = @(Cc, f, Rs) (1 + (2.*pi.*f.*1e9.*Cc.*1e-18.*Rs).^2) ./ ...
                 ((2.*pi.*f.*1e9.*Cc.*1e-18).^2.*Rs);

Rs = 1; %ohms
f = 2:0.1:7; %GHz
Cc= 1:1:1000; %aF
C = 75; %fF
contours = [1 10 50 100 200 1000].*1e-6;
RCtime = zeros(length(f),length(Cc));
for n = 1:length(f)
    RCtime(n,:) = C.*1e-15 .* R(Cc,f(n),Rs);
end

figure;
contourf(f,Cc,log(RCtime'./1e-6))
colormap jet
colorbar

figure;
loglog(Cc, C.*1e-15 .* R(Cc,5,Rs));