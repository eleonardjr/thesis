d=load('time_traces.mat');

time = (d.time - 50e-12)/200; % removing initial transient
idx = time >= 0;

figure; 
subplot(1,2,1); hold on;
plot(time(idx)/1e-12,d.trigger(idx)/1e-6, 'DisplayName','Trigger Tone')
plot(time(idx)/1e-12,d.output(idx)/1e-6, 'DisplayName','Converter Output')
grid off
xlabel('Time (2\pi/\omega_d)');
ylabel('Voltage (\muV)');
legend('show','Location','best');
set(gca,'FontSize',11)

subplot(1,2,2)

dt = load('InductanceSweep_fine.mat');
dnt = load('InductanceSweep_noTrigger_fine.mat');

idx = dt.inductance == 22;
plot(dnt.bias_current/1e-6, (dnt.voltage(:,idx)),'-','DisplayName','No Trigger')
hold on; grid off;
plot(dt.bias_current/1e-6, (dt.voltage(:,idx)),'-','DisplayName','-60 dBm @ 5 GHz')
xlim([0 max(dnt.bias_current/1e-6)])
ylim([-2 max(dnt.voltage(:,idx))])
xlim([0 150])
ylim([-2 25])
set(gca,'FontSize',11)
%xlabel('Bias Current (\muA)')
%ylabel('Converter Voltage (\muV)')
legend('show','Location','best')

figure;
cmax = max(max([dt.voltage dnt.voltage]));
cmin = min(min([dt.voltage dnt.voltage]));
subplot(1,3,1)
contourf(dt.inductance, dt.bias_current/1e-6, smoothdata(dnt.voltage),7)
ylim([60 155])
colormap jet
caxis([cmin cmax])
set(gca,'FontSize',11)
subplot(1,3,2)
contourf(dt.inductance, dt.bias_current/1e-6, smoothdata(dt.voltage),7)
ylim([60 155])
colormap jet
caxis([cmin cmax])
set(gca,'FontSize',11)
subplot(1,3,3)
contourf(dt.inductance, dt.bias_current/1e-6, smoothdata(dt.voltage-dnt.voltage),7)
ylim([60 155])
colormap jet
caxis([cmin cmax])
set(gca,'FontSize',11)
%c=colorbar;
hold on

ind_ref = zeros(length(dt.bias_current),1);
for n=1:length(dt.bias_current)
    ind_ref(n) = 22; % simulated target inductance with fasthenry
end

%plot(ind_ref, dt.bias_current/1e-6, 'w--')