n=load('QP_poisoning_1.6GHz_fit.mat');
p=load('poisoning_data.mat');
t_cut=200; %ns, the nqp fit doesn't work well beyond this in the data here

n_idx=n.data.SFQ_RB_Pulse_Duration<=t_cut;
n_length=n.data.SFQ_RB_Pulse_Duration(n_idx);
n_data=n.data.Extracted_Average_QP_Number(n_idx);

p_idx = p.detune.SFQ_RB_Pulse_Duration<=t_cut;
p_length = p.detune.SFQ_RB_Pulse_Duration(p_idx);
p_detune = p.detune.Extracted_Ramsey_Detuning(p_idx);
p_detune = 2*pi*(p_detune - max(p_detune))./1e-6;
p_detune_er = 2*pi*p.detune.error.Extracted_Ramsey_Detuning(p_idx,1)./1e-6;
p_rate = p.rate.Extracted_Rate_Constant(p_idx)./1e-6;
p_rate_er = p.rate.error.Extracted_Rate_Constant(p_idx,1)./1e-6;

n_interp = interp1(n_length,smoothdata(n_data),p_length);

p_slips = p_length*4*p.detune.SFQ_Frequency;

createFigure; hold on;
plot(n_length, n_data,'o','DisplayName','Extracted from Data');
plot(p_length, n_interp,'x','DisplayName','Smoothed and Interpolated');
xlabel('Poisoning Pulse Length (ns)')
ylabel('Extracted/Interpolated \langle n_{qp}\rangle')


createFigure; hold on;
yyaxis left;
errorbar(n_interp,p_detune,p_detune_er,'-','CapSize',0,'LineWidth',2)
scatter(n_interp,p_detune,[],p_slips,'filled')
xlabel('Interpolated \langle n_{qp} \rangle')
ylabel('Qubit Shift (rad ms^{-1})')

yyaxis right;
errorbar(n_interp,p_rate,p_rate_er,'-','CapSize',0,'LineWidth',2)
scatter(n_interp,p_rate,[],p_slips,'filled')
ylabel('Relaxation Rate \Gamma (ms^{-1})');
xlim([0.05 4.1])

colormap jet;
cbr=colorbar('northoutside');
cbr.Label.String='Phase Slips';
%cbr.Label.Position = cbr.Label.Position+[1 0 0];



