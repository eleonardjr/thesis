p=load('poisoning_data.mat');
r=load('recovery_data.mat');
n=load('QP_poisoning_1.6GHz_fit.mat');
nr=load('nqp_recovery.mat');

%% Poisoning Data

% frequency shift
createFigure; hold on;
subplot(1,2,2)
nslips = 4 * p.detune.SFQ_RB_Pulse_Duration * p.detune.SFQ_Frequency;
errorbar(nslips, p.detune.Extracted_Ramsey_Detuning./1e-3,...
             mean(p.detune.error.Extracted_Ramsey_Detuning,2)./1e-3,...
             'o','MarkerSize',3,'CapSize',0)
xlabel('Phase Slips')
ylabel('Ramsey Detuning (MHz)')
grid off
%set(gca,'FontSize',10)

% T2*
%subplot(1,2,3)
nslips = 4 * p.detune.SFQ_RB_Pulse_Duration * p.detune.SFQ_Frequency;
percent_error = mean(p.detune.error.Extracted_T_White,2) ./ p.detune.Extracted_T_White;
gamma_phi = 1./p.detune.Extracted_T_White;
gamma_err = gamma_phi .* percent_error;
%plotErrorbar(nslips, 1e6./(p.detune.Extracted_T_White),...
%             1e6.*mean(p.detune.error.Extracted_T_White,2))
%plotErrorbar(nslips, 1e6.*gamma_phi, 1e6.*gamma_err)
%xlabel('Phase Slips')
%ylabel('Dephasing Rate (ms^{-1})')
set(gca,'FontSize',11)

% relaxation rate
subplot(1,2,1); hold on;
nslips = 4 * p.rate.SFQ_RB_Pulse_Duration * p.rate.SFQ_Frequency;
errorbar(nslips, p.rate.Extracted_Rate_Constant./1e-6,...
             mean(p.rate.error.Extracted_Rate_Constant,2)./1e-6,...
             'o','MarkerSize',3,'CapSize',0)
errorbar(nslips, 1e6.*gamma_phi, 1e6.*gamma_err,...
    'o','MarkerSize',3,'CapSize',0)
xlabel('Number of Phase Slips')
ylabel('Rate (ms^{-1})')
grid off
set(gca,'FontSize',11)
%set(gca,'FontSize',10)

% the two plotted against each other
createFigure; hold on;
p_rate = (p.rate.Extracted_Rate_Constant - ...
       min(p.rate.Extracted_Rate_Constant))./1e-6;
p_shift = 2*pi*(p.detune.Extracted_Ramsey_Detuning - ...
              max(p.detune.Extracted_Ramsey_Detuning))./1e-6;
p_rate_err = p.rate.error.Extracted_Rate_Constant./1e-6;
p_shift_err = 2*pi*p.detune.error.Extracted_Ramsey_Detuning./1e-6;

h=errorbar(p_rate, p_shift, p_shift_err(:,1), p_shift_err(:,2),...
           p_rate_err(:,1), p_rate_err(:,2), 'k.');
set(h,'CapSize',0)
set(h,'LineWidth',1)

ft = fittype('p1 * x',...
        'independent', 'x', 'dependent', 'y',...
        'coefficients', {'p1'});

opts = fitoptions('Method', 'NonlinearLeastSquares',...
                      'Robust', 'LAR',...
                      'MaxFunEvals',10000,...
                      'MaxIter',10000,...
                      'DiffMaxChange',1,...
                      'Algorithm','Trust-Region');
opts.StartPoint = -0.5;
opts.Display = 'Off';
opts.TolX = 1e-9;
opts.TolFun = 1e-9;
p_fit = fit(p_rate, p_shift, ft, opts);

ci = confint(p_fit, 0.68);
p_fit_err = mean([ci(1,1) - p_fit.p1, p_fit.p1 - ci(2,1)]);
plot(p_rate, p_fit(p_rate), 'k-', 'LineWidth', 0.5)
scatter(p_rate,p_shift,[],nslips,'filled')
colormap jet
cbar=colorbar;
cbar.Label.String='Number of Phase Slips @ 1.6 GHz';
cbar.Label.Rotation = 270;
cbar.Label.Position = cbar.Label.Position + [1 0 0];

leg = legend({'Data',...
    ['Slope = ',num2str(p_fit.p1,2),'\pm',num2str(p_fit_err,1)]});
leg.Location = 'best';
%set(gca,'FontSize',10)
xlabel('Recovery Rate (ms^{-1})')
ylabel('Ramsey Detuning (rad ms^{-1})')

%% Recovery Data

% frequency shift
createFigure; hold on;
subplot(1,2,1)
plotErrorbar(r.detune.Reset_Time,...
             r.detune.Extracted_Ramsey_Detuning./1e-3,...
             mean(r.detune.error.Extracted_Ramsey_Detuning,2)./1e-3)
xlabel('Recovery Time (\mus)')
ylabel('Ramsey Detuning (MHz)')
set(gca,'FontSize',10)

% relaxation rate
subplot(1,2,2)
plotErrorbar(r.rate.Reset_Time, r.rate.Extracted_Rate_Constant./1e-6,...
             mean(r.rate.error.Extracted_Rate_Constant,2)./1e-6)
xlabel('Recovery Time (\mus)')
ylabel('Relaxation Rate (ms^{-1})')
set(gca,'FontSize',10)

% plotted against each other
createFigure; hold on;
r_rate = (r.rate.Extracted_Rate_Constant - ...
       min(r.rate.Extracted_Rate_Constant))./1e-6;
r_shift = 2*pi*(r.detune.Extracted_Ramsey_Detuning - ...
              max(r.detune.Extracted_Ramsey_Detuning))./1e-6;
r_rate_err = r.rate.error.Extracted_Rate_Constant./1e-6;
r_shift_err = 2*pi*r.detune.error.Extracted_Ramsey_Detuning./1e-6;

h=errorbar(r_rate, r_shift, r_shift_err(:,1), r_shift_err(:,2),...
           r_rate_err(:,1), r_rate_err(:,2), 'k.');
set(h,'CapSize',0)
set(h,'LineWidth',1)

r_fit = fit(r_rate, r_shift, ft, opts);

ci = confint(r_fit, 0.68);
fit_err = mean([ci(1,1) - r_fit.p1, r_fit.p1 - ci(2,1)]);
plot(r_rate, r_fit(r_rate), 'k-', 'LineWidth', 0.5)
scatter(r_rate,r_shift,[],r.detune.Reset_Time,'filled')
colormap jet
cbar=colorbar;
%cbar.Label.String='Recovery Time (\mus)';
%cbar.Label.Rotation = 270;
%cbar.Label.Position = cbar.Label.Position + [1 0 0];

leg = legend({'Data',...
    ['Slope = ',num2str(r_fit.p1,4),'\pm',num2str(fit_err,1)]});
leg.Location = 'best';
set(gca,'FontSize',9)
%xlabel('Recovery Rate (ms^{-1})')
%ylabel('Ramsey Detuning (rad ms^{-1})')
xlim([-5 450])
ylim([-800 10])
grid off

%% Plot the two sets together
createFigure; hold on;
h1=errorbar(p_rate, p_shift, p_shift_err(:,1), p_shift_err(:,2),...
           p_rate_err(:,1), p_rate_err(:,2),'o',...
           'DisplayName','Poisoning Data');
set(h1,'CapSize',0)
set(h1,'LineWidth',1)
h2=errorbar(r_rate, r_shift, r_shift_err(:,1), r_shift_err(:,2),...
           r_rate_err(:,1), r_rate_err(:,2),'o',...
           'DisplayName','Recovery Data');
set(h2,'CapSize',0)
set(h2,'LineWidth',1)
leg=legend('show','Location','best');
xlabel('Recovery Rate (ms^{-1})')
ylabel('Ramsey Detuning (rad ms^{-1})')
%set(gca,'FontSize',10)


%% Recovery nqp
createFigure; hold on;

ft = fittype('a * exp(-x/b) + c',...
        'independent', 'x', 'dependent', 'y',...
        'coefficients', {'a', 'b', 'c'});

opts.StartPoint = [3, 17, 0];
recovery_fit = fit(nr.recover_time', nr.nqp', ft, opts);
plot(nr.recover_time, recovery_fit(nr.recover_time),'r-','LineWidth',1)


h1=errorbar(nr.recover_time,nr.nqp,nr.nqper(1,:),nr.nqper(2,:),'ko','MarkerSize',2);
set(h1,'CapSize',0)
set(h1,'LineWidth',0.5)
set(gca,'FontSize',9)
grid off;
ylim([0 3.25])
xlim([14.5 120])

%% make two parametric plots in one figure
figure;
subplot(1,2,1); hold on;
p_rate = (p.rate.Extracted_Rate_Constant - ...
       min(p.rate.Extracted_Rate_Constant))./1e-6;
p_shift = 2*pi*(p.detune.Extracted_Ramsey_Detuning - ...
              max(p.detune.Extracted_Ramsey_Detuning))./1e-6;
p_rate_err = p.rate.error.Extracted_Rate_Constant./1e-6;
p_shift_err = 2*pi*p.detune.error.Extracted_Ramsey_Detuning./1e-6;

h=errorbar(p_rate, p_shift, p_shift_err(:,1), p_shift_err(:,2),...
           p_rate_err(:,1), p_rate_err(:,2), 'k.');
set(h,'CapSize',0)
set(h,'LineWidth',1)

ft = fittype('p1 * x',...
        'independent', 'x', 'dependent', 'y',...
        'coefficients', {'p1'});

opts = fitoptions('Method', 'NonlinearLeastSquares',...
                      'Robust', 'LAR',...
                      'MaxFunEvals',10000,...
                      'MaxIter',10000,...
                      'DiffMaxChange',1,...
                      'Algorithm','Trust-Region');
opts.StartPoint = -0.5;
opts.Display = 'Off';
opts.TolX = 1e-9;
opts.TolFun = 1e-9;
p_fit = fit(p_rate, p_shift, ft, opts);

ci = confint(p_fit, 0.68);
p_fit_err = mean([ci(1,1) - p_fit.p1, p_fit.p1 - ci(2,1)]);
plot(p_rate, p_fit(p_rate), 'k-', 'LineWidth', 0.5)
scatter(p_rate,p_shift,8,nslips/1000,'filled')
colormap jet
cbar=colorbar;
%cbar.Label.String='Number of Phase Slips @ 1.6 GHz';
%cbar.Label.Rotation = 270;
%cbar.Label.Position = cbar.Label.Position + [1 0 0];

leg = legend({'Data',...
    ['Slope = ',num2str(p_fit.p1,2),'\pm',num2str(p_fit_err,1)]});
leg.Location = 'best';
%set(gca,'FontSize',10)
%xlabel('Recovery Rate (ms^{-1})')
%ylabel('Ramsey Detuning (rad ms^{-1})')
set(gca,'FontSize',11)
grid off
ylim([-340 10])
xlim([-5 55])

subplot(1,2,2); hold on;
r_rate = (r.rate.Extracted_Rate_Constant - ...
       min(r.rate.Extracted_Rate_Constant))./1e-6;
r_shift = 2*pi*(r.detune.Extracted_Ramsey_Detuning - ...
              max(r.detune.Extracted_Ramsey_Detuning))./1e-6;
r_rate_err = r.rate.error.Extracted_Rate_Constant./1e-6;
r_shift_err = 2*pi*r.detune.error.Extracted_Ramsey_Detuning./1e-6;

h=errorbar(r_rate, r_shift, r_shift_err(:,1), r_shift_err(:,2),...
           r_rate_err(:,1), r_rate_err(:,2), 'k.');
set(h,'CapSize',0)
set(h,'LineWidth',1)

r_fit = fit(r_rate, r_shift, ft, opts);

ci = confint(r_fit, 0.68);
fit_err = mean([ci(1,1) - r_fit.p1, r_fit.p1 - ci(2,1)]);
plot(r_rate, r_fit(r_rate), 'k-', 'LineWidth', 0.5)
scatter(r_rate,r_shift,8,r.detune.Reset_Time,'filled')
colormap jet
cbar=colorbar;
%cbar.Label.String='Recovery Time (\mus)';
%cbar.Label.Rotation = 270;
%cbar.Label.Position = cbar.Label.Position + [1 0 0];

leg = legend({'Data',...
    ['Slope = ',num2str(r_fit.p1,4),'\pm',num2str(fit_err,1)]});
leg.Location = 'best';
set(gca,'FontSize',11)
%xlabel('Recovery Rate (ms^{-1})')
%ylabel('Ramsey Detuning (rad ms^{-1})')
xlim([-5 450])
ylim([-800 10])
grid off

%% four panel QP plot
figure;
subplot(2,2,3)
nslips = 4 * p.detune.SFQ_RB_Pulse_Duration * p.detune.SFQ_Frequency;
errorbar(nslips, p.detune.Extracted_Ramsey_Detuning./1e-3,...
             mean(p.detune.error.Extracted_Ramsey_Detuning,2)./1e-3,...
             'o','MarkerSize',3,'CapSize',0)
xlabel('Phase Slips')
ylabel('Ramsey Detuning (MHz)')
axis tight
grid off
%set(gca,'FontSize',10)

% T2*
%subplot(1,2,3)
nslips = 4 * p.detune.SFQ_RB_Pulse_Duration * p.detune.SFQ_Frequency;
percent_error = mean(p.detune.error.Extracted_T_White,2) ./ p.detune.Extracted_T_White;
gamma_phi = 1./p.detune.Extracted_T_White;
gamma_err = gamma_phi .* percent_error;
%plotErrorbar(nslips, 1e6./(p.detune.Extracted_T_White),...
%             1e6.*mean(p.detune.error.Extracted_T_White,2))
%plotErrorbar(nslips, 1e6.*gamma_phi, 1e6.*gamma_err)
%xlabel('Phase Slips')
%ylabel('Dephasing Rate (ms^{-1})')
set(gca,'FontSize',11)

% relaxation rate
subplot(2,2,1); hold on;
nslips = 4 * p.rate.SFQ_RB_Pulse_Duration * p.rate.SFQ_Frequency;
errorbar(nslips, p.rate.Extracted_Rate_Constant./1e-6,...
             mean(p.rate.error.Extracted_Rate_Constant,2)./1e-6,...
             'o','MarkerSize',3,'CapSize',0)
errorbar(nslips, 1e6.*gamma_phi, 1e6.*gamma_err,...
    'o','MarkerSize',3,'CapSize',0)
xlabel('Number of Phase Slips')
axis tight
ylabel('Rate (ms^{-1})')
grid off
set(gca,'FontSize',11)
%set(gca,'FontSize',10)

subplot(2,2,2); hold on;
errorbar(r.rate.Reset_Time, r.rate.Extracted_Rate_Constant./1e-6,...
             mean(r.rate.error.Extracted_Rate_Constant,2)./1e-6,...
             'o','MarkerSize',3,'CapSize',0)
xlabel('Recovery Time (\mus)')
axis tight
ylabel('Relaxation Rate (ms^{-1})')
set(gca,'FontSize',11)
grid off
percent_error = mean(r.detune.error.Extracted_T_White,2) ./ r.detune.Extracted_T_White;
gamma_phi = 1./r.detune.Extracted_T_White;
gamma_err = gamma_phi .* percent_error;
errorbar(r.rate.Reset_Time, 1e6.*gamma_phi, 1e6.*gamma_err,...
    'o','MarkerSize',3,'CapSize',0)

subplot(2,2,4)
errorbar(r.detune.Reset_Time,...
             r.detune.Extracted_Ramsey_Detuning./1e-3,...
             mean(r.detune.error.Extracted_Ramsey_Detuning,2)./1e-3,...
             'o','MarkerSize',3,'CapSize',0)
xlabel('Recovery Time (\mus)')
axis tight
ylabel('Ramsey Detuning (MHz)')
grid off
set(gca,'FontSize',11)