d=loadMeasurementData('poisoning_rate_data.mat');

num_slips = d.SFQ_RB_Pulse_Duration * d.SFQ_Frequency * 4;
pi2_slips = d.SFQ_RB_Pi_Over_2_Duration * d.SFQ_Frequency * 4;
pi_slips = pi2_slips * 2;


lin_idx = num_slips >= 256; % hand chosen based on data

linfit = fit(num_slips(lin_idx),d.Extracted_Average_QP_Number(lin_idx),...
             'poly1','Robust','LAR');
ci = confint(linfit,0.68);
err=max([linfit.p1-ci(1,1) ci(2,1)-linfit.p1]);

h=figure; hold on;
plot(num_slips(lin_idx)/100,linfit(num_slips(lin_idx)),'r-','LineWidth',0.75)
% set(gca,'FontSize',9)
h1=errorbar(num_slips/100,d.Extracted_Average_QP_Number,...
    d.error.Extracted_Average_QP_Number(:,1),...
    d.error.Extracted_Average_QP_Number(:,2),'ko','MarkerSize',2);
set(h1,'CapSize',0)
set(h1,'LineWidth',0.5)
set(gca,'FontSize',9)
grid off;
xlim([0 6.50])
ylim([0 0.75])
xticks([0 2 4 6])
plot((0:1:pi_slips)/100,0.6*ones(size(0:1:pi_slips)),'-')
plot((0:1:pi2_slips)/100,0.5*ones(size(0:1:pi2_slips)),'-')
%xlabel('Number of Phase Slips')
%ylabel('Extracted \langle n_{qp}\rangle')

ax1_pos = h.Position; % position of first axes
ax2 = axes('Position',ax1_pos,...
    'XAxisLocation','top',...
    'YAxisLocation','right',...
    'Color','none',...
     'XColor','k','YColor','k');




