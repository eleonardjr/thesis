n=load('QP_poisoning_1.6GHz_fit.mat');

createFigure; hold on;

T1=2.33e1; %in us
t=n.data.Qubit_Drive_to_Readout/1000; % t in us

% no poison data
npdata = mat2gray(n.data.Average_Poor_Mans_Occupation(1,:));
npfit = QPDecayFit(t,npdata,true,2.33e1);

% the poison pulse data
pdata = mat2gray(n.data.Average_Poor_Mans_Occupation(21,:));
pfit = QPDecayFit(t,pdata,true,2.33e1);


% the two fits



plot(t,npfit(t), '-','LineWidth',1)
plot(t,pfit(t), '-','LineWidth',1)
set(gca,'ColorOrderIndex',1)
%plot(t, npdata, 'o','MarkerSize',1);
%plot(t,pdata, 'o','MarkerSize',1);
%nph = scatter(t,npdata,6,'MarkerFaceColor',[0 0.447 0.741],'LineWidth',.25);
nph = scatter(t,npdata,12,'MarkerFaceColor','white','LineWidth',.25);
nph.MarkerFaceAlpha = .2;
nph.MarkerEdgeAlpha = .5;
ph = scatter(t,pdata,12,'MarkerFaceColor',[0.85 0.325 0.098],'LineWidth',.25);
ph.MarkerFaceAlpha = .2;
ph.MarkerEdgeAlpha = .5;
set(gca,'YScale','log')
set(gca,'FontSize',8)
grid off
xlim([0 100])
ylim([0 1])


function f = QPDecayFit(x, y, t1_bool, t1_val)
    opts = fitoptions('Method', 'NonlinearLeastSquares',...
                      'Robust', 'LAR',...
                      'MaxFunEvals',10000,...
                      'MaxIter',10000,...
                      'DiffMaxChange',1,...
                      'Algorithm','Trust-Region');
    % opts.Display = 'Off';
    opts.TolX = 1e-9;
    opts.TolFun = 1e-9;
    
    if ~t1_bool
        opts.StartPoint = [y(1) - y(end), 3 / ((max(x) - min(x) + 9 * eps)),...
                y(end), 3 / (max(x) - min(x) + 9 * eps), 1];
        opts.Lower = [min(y), 0, min(y), 0, 0];
        opts.Upper = [max(y), 5*max(x), max(y), 100*max(x), 10];
        f = fit(x(:), y(:), 'a * exp(n_qp * (exp(-d * x) - 1) - b*x) + c', opts);
    else
        numt1s = length(t1_val);
        if numt1s == 1
            opts.StartPoint = [y(1) - y(end), y(end), 0.5 / (max(x) - min(x) + 9 * eps), 1];
            opts.Lower = [min(y), min(y), 0, 0];
            func = ['a * exp(n_qp * (exp(-d * x) - 1) -x/',num2str(t1_val),') + c'];
        elseif numt1s == 2
            opts.StartPoint = [y(1) - y(end), y(end), 1];
            opts.Lower = [min(y), min(y), 0];
            func = ['a * exp(n_qp * (exp(-x/',num2str(t1_val(2)),') - 1) -x/',num2str(t1_val(1)),') + c'];
        end
        f = fit(x(:), y(:), func, opts);
    end
end