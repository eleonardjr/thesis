d=load('sfq_sequence_FFToutput_full.mat');
figure; 
subplot(1,2,1); hold on;
n_lines = [5 10 100];
for n=1:length(n_lines)
    idx = find(d.n_pulses == n_lines(n));
    plot(d.freqs(1:d.top_idx)/1e9, d.norm_amp(idx,1:d.top_idx),...
        'DisplayName',['n = ',num2str(n_lines(n))])
end
grid off
xlabel('Frequency (GHz)')
ylabel('Normalized Amplitude')
legend('show','Location','best')
xlim([2 8])
set(gca,'FontSize',12)

subplot(1,2,2)
loglog(d.n_pulses, smoothdata(d.f21_amp));
xticks([0 10 100])
set(gca,'FontSize',12)
grid off