d=load('RamseyOutput_ufine.mat');
lambda = 0.004;
tpi2 = 30;
tr = 3*tpi2;
tidle = max(d.idle_time_in_periods);
tidle = 400;

step = d.idle_time_in_periods(2) - d.idle_time_in_periods(1);
t = 0:step:2*30+max(d.idle_time_in_periods);
shift = zeros(length(t),1);

for n = 1:length(t)
   if t(n) < tpi2
       shift(n) = lambda * t(n) / tpi2;
   elseif t(n) >= tpi2
       if t(n) < tpi2 + tidle
           shift(n) = lambda * exp(-(t(n)-tpi2)/tr);
       elseif t(n) >= tpi2+tidle
           shift(n) = lambda * (exp(-(tidle-tpi2)/tr) + (t(n)-tidle-tpi2)/tpi2);
       end
   end
end

shift = 1-shift;
t=t-tpi2;



figure;
subplot(1,2,1)
plot(shift, t)
grid off
set(gca,'FontSize',12)
axis tight
xlim([min(d.norm_drive_freq) max(d.norm_drive_freq)])
ylim([-30 430])

subplot(1,2,2)
imagesc(d.norm_drive_freq,d.idle_time_in_periods,d.p1')
set(gca,'FontSize',12)
axis tight
grid off
set(gca,'YDir','normal')
colormap jet
ylim([0 400])

