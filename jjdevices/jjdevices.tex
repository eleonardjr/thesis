% \svnidlong{$LastChangedBy$}{$LastChangedRevision$}{$LastChangedDate$}{$HeadURL: http://freevariable.com/dissertation/branches/diss-template/frontmatter/frontmatter.tex $}
% \vcinfo{}
\chapter{Superconductive Devices}
\label{chap:sce}
Following the discovery of superconductivity of Hg in liquid helium (LHe) by Onnes in 1911~\cite{Onnes1911aa,Onnes1911ab} and the successful theoretical treatment of microscopic superconductivity by Bardeen, Cooper, and Schrieffer (BCS) in 1957~\cite{Bardeen:1957aa}, a great wealth of research has been done insofar as what sorts of devices can be constructed which take advantage of the superconductive phenomenon. In this Chapter, we present a rather non-exhaustive overview of select low-temperature superconducting devices as well as their physical properties as they are relevant to this work. Here, we will not go into great detail on the nuances of neither superconductivity nor the effect of magnetic fields on superconductors or superconductive devices in favor of highlighting the ideas and facets of the technologies as they are contextually relevant for this work. Assuming the reader's interest, the author suggests Tinkham's introductory text on superconductivity~\cite{Tinkham} for the uninitiated with follow-up texts specializing on the Josephson effect by Barone~\cite{Barone} and Likharev~\cite{Likharev} for the advanced reader.

This Chapter is divided as follows: Section~\ref{sec:scjj} introduces the workhorse device of most superconducting devices, the Josephson junction (JJ). Then, in Section~\ref{sec:qdev}, we give an overview of quantum devices based on superconducting devices, and, most notably, JJs. In Section~\ref{sec:sce}, we provide a brief overview of the state of modern superconducting electronics. Finally, in Section~\ref{sec:hybrid}, we summarize this Chapter and help place it firmly in the context of superconducting quantum information processing.

\section{Josephson Junctions}\label{sec:scjj}

\begin{figure}[t]
\includegraphics[width=\textwidth]{jjdevices/jj.pdf}
\caption[Josephson junction device overview with example realizations.]{\label{fig:jj} \textbf{(a)}~JJ block diagram of two superconductors (blue), each with respective superconducting wavefunction $\Psi_i$ and phase $\theta_i$, interrupted by a weak link (grey). The phase difference between the two superconductors across the weak link is defined as $\delta\equiv\theta_2-\theta_1$. \textbf{(b)}~Circuit representation of the above JJ with critical current $I_0$ and self-capacitance $C_{{J}}$ (an obvious addition given the geometry above). \textbf{(c)}~SEM image of an overlap-style Al/AlOx/Al JJ fabricated using a standard Dolan bridge process~\cite{Dolan:1977aa} in an electron-beam lithography tool followed by double-angle evaporation of Al electrodes. This technology is useful for achieving low critical current densities ($J_c\approx10$~A/cm$^2$) appropriate for modern qubit devices. \textbf{(d)}~SEM image of a via-style JJ fabricated by interleaving two superconducting Nb layers with a thin AlOx barrier. (top) Top-down view of the device. (bottom)Cross-section showing the layer stack. Here, blue is Nb, orange is SiOx, and the thin black line between the two upper Nb layers is a $\sim$nm-thick AlOx barrier. This technology is useful for achieving high $J_c$ ($\gtrsim10$~kA/cm$^2$), which is typical in modern SCE circuitry.}
\end{figure}

The workhorse device in the modern-day field of superconducting devices is without doubt the JJ~\cite{Josephson:1962aa,Josephson:1974aa} -- a device composed of two superconducting wires interrupted by a weak link. If the mitochondrion is the ``Powerhouse of the [living] cell''~\cite{Siekevitz:1957aa}, then the JJ is the ``Powerhouse of superconductive devices.'' Whether one is interested in the construction of quantum or classical superconducting circuitry, the JJ is ubiquitous due to its wide variety of use cases and constructions. In this Section, we will provide a brief outline of the dynamics of a single JJ in a few situations to serve as groundwork for the following Sections of this Chapter.

\subsection{Josephson Relations}

JJs are constructed by interleaving two superconducting materials with a weak link. The materials used for the superconductors and weak links largely have an impact on the fabrication needs of a specific technology while not having a significant impact on the mathematical treatment of the JJ. Josephson's theoretical work showed two specific relations for these devices: that the current through such a device and the voltage across it could be described by 
\begin{align}
I\left(\delta\right)&=I_c\sin\left(\delta\right)\label{eq:jji}\\
V\left(\delta\right)&=\frac{\Phi_0}{2\pi}\frac{d\delta}{dt}\label{eq:jjv},
\end{align}
respectively, where $\delta$ is the phase difference of the superconducting wavefunction on either side of the JJ [shown diagramatically in Fig.~\refpart{fig:jj}{a}] and $\Phi_0\equiv h/2e$ is the magnetic flux quantum. The Josephson effect was first experimentally verified by Anderson~\cite{Anderson:1963aa} after which a number of different constructions including those using an insulator between the two superconductors (so-called Superconductor-Insulator-Superconductor, or SIS), a normal metal (SNS), or simply a narrow constriction of the superconducting material (SsS) -- all of which obey the above relations irrespective of which construction is employed. Differentiating Eq.~(\ref{eq:jji}) and substituting Eq.~(\ref{eq:jjv}) for $d\delta/dt$, one finds
\begin{align}
V\left(\delta\right)&=\frac{\Phi_0}{2\pi I_0\cos\delta}\frac{dI}{dt},
\end{align}
or a voltage drop with a linear dependence on $dI/dt$, implying that the pre-factor here is an inductance
\begin{align}
L_J\left(\delta\right)&=\frac{\Phi_0}{2\pi I_0\cos\delta}\equiv\frac{L_{J0}}{\cos\delta}\label{eq:jjind},
\end{align}
the so-called Josephson inductance. This makes the JJ a dissipation-free nonlinear inductor, which are the key aspects of the device in its use in superconducting qubits as well as in large-scale SCE components.

\subsection{Josephson Energy\label{sec:jjcurrbias}}

Since the JJ can be described as an inductor, it is then straightforward to imagine that the JJ is capable of storing energy. This energy is found by integrating the product of Eqs.~(\ref{eq:jji},\ref{eq:jjv}):
\begin{align}
U_J&=\int I\left(\delta\right)V\left(\delta\right)dt\nonumber\\
&=\frac{I_0\Phi_0}{2\pi}\int\sin\left(\delta\right)\frac{d\delta}{dt}dt\nonumber\\
&=-\frac{I_0\Phi_0}{2\pi}\cos\delta\equiv-E_J\cos\delta, \label{eq:jjpot}
\end{align}
where $E_J$ is defined as the Josephson energy and the integration constant has been ignored since it will only contribute to a global offset and not the dynamics of the circuit. Coupling Eq.~(\ref{eq:jjpot}) with Fig.~\refpart{fig:jj}{b}, we can now write an effective Hamiltonian for a single JJ as 
\begin{align}
\mathcal{H}_J&=E_C+U_J,
\end{align}
where $E_C$ is the charging energy of the capacitor shunting the JJ. Substituting in the circuit quantities, the Hamiltonian becomes
\begin{align}
\hat{\mathcal{H}}_J&=\frac{\hat{Q}^2}{2C_{J}} - E_J\cos\hat{\delta}=4E_C\hat{n}^2-E_J\cos\hat{\delta},\label{eq:jjham}
\end{align}
where the charge $Q$ on the capacitor $C_{{J}}$ and the phase difference $\delta$ across the JJ have both been promoted to quantum mechanical operators, $\hat{n}=\hat{Q}/2e$ is the operator for the number of Cooper-pairs on the positive side of the JJ, and $E_C=e^2/2C_J$ is the charging energy of $C_J$. Here, $\hat{n}$ and $\hat{\delta}$ obey the canonical commutation relation
\begin{align}
\left[\hat{\delta},\hat{n}\right]&=i,
\end{align}
Now, a slightly more interesting circuit is the current-biased JJ as shown in Fig.~\refpart{fig:jjquant}{a}. This modifies the JJ potential energy to the form
\begin{align}
U_J&=\int I\left(\delta\right)V\left(\delta\right)dt=-E_J\left( \cos\delta +\delta I_b/I_0\right),\label{eq:wash}
\end{align}
where $I_b$ is the JJ bias current. This potential energy provides one of the most clear methods through which the dynamics of a JJ can be understood. In Fig.~\refpart{fig:jjquant}{a}, we plot Eq.~(\ref{eq:wash}), known as the ``tilted washboard potential,'' for three different values of $i\equiv I_b/I_0$ and we superpose a so-called ``phase particle'' (black circle) atop two of the lines ($i=\{0.5,1\}$) to illustrate potential dynamics of $\delta$. Using this illustration, it is plain to see that for a sufficiently small kinetic energy of the phase particle, it can become trapped in a well of $U(\delta)$ and simply slosh back and forth. However, once $I_b\geq I_0$, the wells of the potential no longer have a local minima into which the phase particle may settle, and instead it will continue to free-evolve\footnote{The author suggests~\cite{CLARKE:1988aa} for more perspective on this analogy.}.

\begin{figure}[t]
\includegraphics[width=\textwidth]{jjdevices/jj_circuits.pdf}
\caption[Basic Josephson junction circuits with sample simulation and data.]{\label{fig:jjquant} \textbf{(a)} (circuit) Simple current-biased JJ with the JJ self-capacitance for completeness. (plot) The potential energy $U_J$ of the JJ for different bias currents as prescribed by Eq.~(\ref{eq:wash}). Here, the fictitious phase particle is drawn in black rolling along the $i_b=\{0.5,1\}$ curves; using this analogy, it is simple to see that there are local minima in which the phase particle can become trapped so long as $i_b<1$ with the alternate being a phase particle in free fall, resulting in a continuous evolution of $\delta$. \textbf{(b)} (circuit) A resistively and capacitively shunted JJ (RCSJ) including a voltmeter $V$ to monitor the response of the circuit to changing bias $I_b$. (left plot) Sample $IV$ curve from an unshunted JJ ($R_s\rightarrow\infty$). Here, the critical current is $I_0\sim30\;\mu\textrm{A}$, retrapping current $I_r\sim\;3\mu$A and the superconducting gap of the JJ is $2\Delta\sim2.9$~meV, $\sim3\%$ lower than bulk Nb. (right plot) Over-damped JJ shunted by a resistance $R_s=3.3\;\Omega$ with critical current $I_0\approx48\;\mu$A. Here, we see the voltage response is single-valued for all current bias space.}
\end{figure}

\subsection{Over- vs. Under-damped JJs (The RCSJ Model)}

A shunt resistor in parallel with a JJ [depicted in Fig.~\refpart{fig:jjquant}{b}] will induce different JJ dynamics with regard to the aforementioned phase particle on a tilted washboard; these dynamics are well described by the \textbf{R}esistor and \textbf{C}apacitor \textbf{S}hunted \textbf{J}unction (RCSJ) model~\cite{McCumber:1968aa,Stewart:1968aa}. Current conservation for such a circuit gives 
\begin{align}
I_b&=I_J+I_C+I_R\\
&=I_0\sin\delta + C_J\frac{dV}{dt} + \frac{V}{R_s},
\end{align}
where $R_s$ is the shunt resistance and $V$ is the voltage across the JJ. Here, we are ignoring the non-zero thermal noise provided by $R$ at finite temperature. Using Eq.~(\ref{eq:jjv}), dimensionless time $\tau=2\pi I_0R_st/\Phi_0$, and dimensionless current $i=I/I_0$, one re-writes this as
\begin{align}
\beta_C\ddot{\delta}&=i_b-\sin\delta-\dot{\delta},
\end{align}
where $\beta_C=2\pi I_0C_JR_s^2/\Phi_0$ is known as the Stewart-McCumber damping parameter~\cite{McCumber:1968aa,Stewart:1968aa} and governs the latching dynamics of the circuit. Perhaps more naturally, it is written as a ratio of the $RC$ time constant to the competing $L/R$ time constant provided by the Josephson inductance Eq.~(\ref{eq:jjind})
\begin{align}
\beta_C&\stackrel{\textrm{def}}{=}\frac{\tau_{RC}}{\tau_{J}}=\frac{R_sC_J}{L_{J0}/R_s}=\frac{R_sC}{(\Phi_0/2\pi I_0)/R_s}=\frac{2\pi}{\Phi_0}I_0C_JR_s^2.
\end{align}
This provides an avenue for a qualitative understanding of $\beta_C$: \underline{if $\beta_C\geq1$}, then the characteristic trapping time of the $R_sC_J$ tank circuit is slower than that of the competing $L_J/R_s$ time -- this implies that the $RL$ oscillator dynamics will dominate, and since the $L$ is representative of the JJ, this will cause the phase particle in Fig.~\refpart{fig:jjquant}{a} to evolve unchecked for sufficiently steep slopes even if $I_b/I_0<1$. This is called the ``latched'' state, in which a single $2\pi$ evolution of $\delta$ will result in many more $2\pi$ oscillations until the slope of the tilt is significantly reduced; this will ``retrap'' the phase particle into one of the potential wells. Conversely, \underline{if $\beta_C<1$}, the $R_sC_J$ circuit clocks in faster than the inductive JJ, dominating the dynamics when $I_b<I_0$. Here, it is possible to induce a single $2\pi$ phase evolution of our fictitious particle in Fig.~\refpart{fig:jjquant}{a} by dynamically modulating $I_b$ to (or slightly above) $I_0$ for a very brief amount of time after which the JJ returns to the supercurrent state.

An experiment one may perform to ascertain whether or not a JJ is over- or under-damped is described as follows [using the circuit in Fig.~\refpart{fig:jjquant}{b}]:
\begin{enumerate}
\item Connect a JJ with parallel resistor to a variable current source $I_b$ and monitor the voltage $V$ across the JJ
\item Sweep $I_b$ up from zero while monitoring the voltage across the JJ until a large step appears at $I_0$ followed by a resistive segment
\item Sweep $I_b$ back down to zero, and plot the two directions together; a JJ with single-valued (multi-valued) voltage responses for similar values of $I_b$ is over-damped with $\beta_C<1$ (under-damped with $\beta_C\geq1$). In the multi-valued case, the current at which the voltage drops back down to zero is called the retrapping current $I_r$.
\end{enumerate}
Sample data from such an experiment for both the over- and under-damped case is shown in Fig.~\refpart{fig:jjquant}{b}.

\subsection{Superconducting QUantum Interference Devices (SQUIDs)}
\label{sec:squids}

Up to this point, a rather glaring omission has been made in the discussion of JJs: the effect of a magnetic field on a JJ circuit. A hallmark device one is able to construct using JJs is a superconducting quantum interference device (SQUID). There are two flavors of SQUID: the rf SQUID comprising a single JJ interrupting an inductor loop and the dc SQUID comprising an inductor loop which is interrupted by two JJs in parallel. Replacing the single JJ of Fig.~\refpart{fig:jjquant}{a} with two JJs in parallel, each with critical current $I_0/2$, does nothing to change the expected dynamics of the circuit. However, in the presence of an external magnetic flux $\Phi_e$, or if the two JJ critical currents are non-equal, special considerations must be made. The current through the two JJ loop would then be written (ignoring the JJ capacitance)
\begin{align}
I_b &= \sum_nI_{0,n}\sin\delta_n=I_{0,1}\sin\delta_1+I_{0,2}\sin\delta_2,
\end{align}
where the continuity of the superconducting wavefunction in the loop stipulates that the two JJ phases are related by
\begin{align}
\delta_2-\delta_1&=2\pi\frac{\Phi}{\Phi_0}\label{eq:phasecont}
\end{align}
where $\Phi$ is the total flux through the loop comprising the two parallel JJs and can be written as $\Phi=\Phi_e+LI_l$ in which $I_l=(I_2-I_1)/2$ is the circulating current in the JJ loop of inductance $L$. Using this, and assuming $I_{0,1}=I_{0,2}\equiv I_0$, the dc SQUID overall critical current is modulated by
\begin{align}
I_c\left(\Phi_e\right)&=2I_0\cos\left(\frac{\pi\Phi_e}{\Phi_0}\right),
\end{align}
implying immediately that the dc SQUID is useful as a magnetometer with exceedingly high sensitivity~\cite{Barone,Tinkham}.

\subsubsection*{dc SQUID Hamiltonian}

The Hamiltonian of a dc SQUID comprising two JJs, when considering only the Josephson energies, can be written as
\begin{align}
H_{J}&=U_{J1}+U_{J2}\nonumber\\
&=-E_{J1}\cos\delta_1-E_{J2}\cos\delta_2,
\end{align}
where $\delta_1,\delta_2$ are constrained by Eq.~(\ref{eq:phasecont}). Defining an effective phase $\delta=(\delta_1+\delta_2)/2$ and Josephson energy $E_{J\Sigma}=E_{J1}+E_{J2}$, the Hamiltonian becomes
\begin{align}
\hat{\mathcal{H}}_J&=-E_{J\Sigma}\cos\left(\frac{\pi\Phi}{\Phi_0}\right)\sqrt{1+d^2\tan^2\left(\frac{\pi\Phi}{\Phi_0}\right)}\cos\left(\hat{\delta}-\delta_0\right),
\end{align}
where $d\equiv (I_2-I_1)/(I_2+I_1)$ is a term representing the asymmetry of the two JJs in the loop and $\tan\delta_0=d\tan(\pi\Phi/\Phi_0)$ is a term which can be discarded by a variable change for a constant flux $\Phi$. This means that the results of the above section concerning single JJ circuits can still be applied to an asymmetric dc SQUID circuit so long as the flux is constant by replacing the JJ energy with~\cite{Koch:2007aa}
\begin{align}
E_J&\rightarrow E_{J\Sigma}\cos\left(\frac{\pi\Phi}{\Phi_0}\right)\sqrt{1+d^2\tan^2\left(\frac{\pi\Phi}{\Phi_0}\right)}.\label{eq:Eassym}
\end{align}
This expression is useful when considering the energy levels of superconducting qubits which employ a dc SQUID (e.g. the split transmon, which is the style of device employed for the work covered in Chapter~\ref{chap:sfq}). 

\section{Superconducting Qubits}\label{sec:qdev}

Given that a JJ is a dissipationless nonlinear circuit element, it follows that a variety of interesting devices which may be treated quantum mechanically can be constructed using it along with other dissipationless items such as inductors and capacitors. One of the most important requirements for a qubit is that the transition between the two states being used as the computational manifold is able to be addressable without exciting the system to an undesired level (this is alluded to in Sec.~\ref{sec:qbint}). It is for this reason that a quantum harmonic oscillator (QHO) is a poor choice as the level spacings between transitions are all degenerate in energy. However, if one is able to introduce a nonlinearity into the potential, the oscillator may become anharmonic (i.e., $E_{21}\neq E_{10}$), implying that the ground and first excited state of the oscillator may be treated to first order as a two-level system; it is convenient in many ways to use a JJ as this nonlinearity in superconducting quantum circuits. What follows in this Section is a brief overview of some traditional superconducting qubit circuits based on JJs and some of their respective properties, advantages, and shortfalls, with special care taken in Sec.~\ref{sec:jjtrans} in discussing the transmon since it was the flavor of qubit used for the work in this thesis. While enough detail for the other flavors of qubits will be shown to provide context for this work, the advanced reader will be interested in reading some of the wealth of literature resulting from the last 20 years of research of these devices and their properties, which has been reviewed from time to time since the early 2000's~\cite{Makhlin:2001aa,Clarke:2008aa,Devoret:2013aa,Oliver:2013aa,Gambetta:2017aa,Wendin:2017aa,Liu:2018aa}. Here, we will readily make use of circuit quantum electrodynamics nomenclature, a brief introduction to which is presented later, in Section~\ref{sec:cqed}.

\begin{figure}[t]
\includegraphics[width=\textwidth]{jjdevices/jj_qb.pdf}
\caption[Example Josephson junction qubit circuits]{\label{fig:jjqb} \textbf{(a)} Circuit for a basic CPB with single bias voltage $V_g$. The ``box'' -- or island -- is highlighted in blue. The voltage across the JJ is written as $V=2\pi\dot{\delta}/\Phi_0\equiv\dot{\phi}$. \textbf{(b)} Micrograph of the first CPB which was coherently manipulated by electrical signals. Here, the CPB island is connected to a reservoir by two JJs in parallel and is capacitively control by both a dc gate ($V_g$) and a rf pulse gate for coherent control. The state of the CPB is measured using a probe JJ. Image direct from~\cite{Nakamura:1999aa}. \textbf{(c)} Simplified circuit diagram for a simple current-biased phase qubit. The potential energy landscape for this circuit is described by Eq.~(\ref{eq:wash}) and shown in Fig.~\refpart{fig:jjquant}{a}. \textbf{(d)} Micrograph of a relatively modern phase qubit device with various additional circuit elements labeled. Here, the phase qubit is shunted by an explicit capacitor and measured via SQUID readout. Image direct from~\cite{Martinis:2008aa}. \textbf{(e)} Circuit diagram for a simple flux qubit wherein a three JJ SQUID is biased by $\Phi_b=MI_b$. \textbf{(f)} SEM micrograph of the first flux qubit in which coherent operations were performed and measured. The qubit circuit comprises the loop on right right while the loop on the left constitutes the measurement circuit for this particular device. Image direct from~\cite{Chiorescu:2003aa}.}
\end{figure}

\subsection{Cooper-pair box}

The Cooper-pair box (CPB)~\cite{Bouchiat:1998aa} (also known as a charge qubit) was the first solid-state device in which a macroscopic quantum state was coherently manipulated using electrical signals\footnote{In the year prior to Nakamura's 1999 work in coherently manipulating the state of a CPB~\cite{Nakamura:1999aa}, another successful effort resulted in the coherent manipulation of a solid-state quantum dot device using optical excitations instead of electrical signals~\cite{Bonadeo:1998aa}.}~\cite{Nakamura:1999aa}. In this style of qubit, the ``macroscopic'' degree of freedom which is to be coherently manipulated is the number of excess Cooper-pairs $n$ which reside on a superconducting metal island which is connected to a Cooper-pair bath via a JJ [depicted in Fig.~\refpart{fig:jjqb}{a}]. The charging energy of the island is similar to before
\begin{align}
E_C&=\frac{e^2}{2C_{\Sigma}}
\end{align}
except now the total island capacitance includes both the gate capacitor $C_g$ as well as the JJ self-capacitance $C_J$ as $C_{\Sigma}=C_g+C_J$. The JJ energy remains unchanged, but there is now an additional term to the kinetic and potential energy due to $C_g$, making the Hamiltonian
\begin{align}
\mathcal{H}_{\textrm{CPB}}&=Q\dot{\phi}-(T-U)=Q\dot{\phi}-\left[\underbrace{\frac{C_g\dot{\phi}^2}{2}+\frac{C_J\dot{\phi}^2}{2}}_{\textrm{kinetic}}-\underbrace{\left[-E_J\cos\frac{2\pi\phi}{\Phi_0}+V_gQ_g\right]}_{\textrm{potential}}\right].
\end{align}
Using the conjugate momentum $Q=\partial\mathcal{L}/\partial\dot{\phi}$, the total capacitance $C_{\Sigma}=C_g+C_J$, defining the charge $Q_g=(-\dot{\phi})V_gC_g$, and elevating $Q$ and $\delta$ to operators, this is written in a simple form
\begin{align}
\hat{\mathcal{H}}_{\textrm{CPB}}&=\frac{\left(\hat{Q}-C_gV_g\right)^2}{2C_{\Sigma}}+E_J\cos\hat\delta.\label{eq:cpbbase}
\end{align} 
Through a change of basis, this can be written in a more convenient form. The ingredients needed are: define a number operator $\hat{n}=\hat{Q}/2e$ representing the number of Cooper-pairs on the island, $n_g=C_gV_g/2e$ for the bias circuit, and also that the $\cos\hat{\delta}$ term for the JJ coupling energy can be re-written using the relationship $e^{\pm i\hat{\delta}}\ket{n}=\ket{n\pm1}$. We can now re-write Eq.~(\ref{eq:cpbbase}) in matrix form as~\cite{Bouchiat:1998aa}
\begin{align}
\hat{\mathcal{H}}_{\textrm{CPB}} &= \sum_n\left[4E_C\left(n-n_g\right)^2\ket{n}\bra{n} - \frac{E_J}{2}\left(\ket{n+1}\bra{n}+\ket{n-1}\bra{n}\right)\right].\label{eq:cpb}
\end{align}
For typical qubits of this style, it is generally a goal to make devices satisfying $E_C\gg E_J$ such that the system is very sensitive to small differences in $\hat{n}$. However, while this was required such that the system could be controlled, it made the device impractical for large-scale systems due to both charge noise and quasiparticle (unbound electrons/holes in the superconductor) tunneling to/from the island causing system instabilities and fast decoherence. As far as the author is aware, no major efforts at present are attempting to further advance CPB technology.

\subsection{Phase qubit}

One design that was hailed for its simplicity in the early days of superconducting qubits was the current-biased phase qubit, which saw its first coherent oscillations in devices tested in the early 2000s~\cite{Ramos:2001aa,Martinis:2002aa}. This device has been already described in Sec.~\ref{sec:jjcurrbias} wherein it is shown that the Hamiltonian of the circuit can be written such that the potential energy of the circuit includes wells which tilt according to a dc bias current, allowing the states of the wells to be tuned into operable regimes. The energy structure in the wells is not purely quadratic, giving rise to an anharmonic structure [i.e., $E_{21}\neq E_{10}$; depicted in Fig.~\refpart{fig:jjquant}{c}] between energy levels which is tunable via adjusting $I_b$.

In the early tests, qubit frequencies tested were around 2-7~GHz~\cite{CLARKE:1988aa,Martinis:2002aa} with coherence times in the 20~ns range~\cite{Martinis:2002aa}. In the years that followed, it was shown the the most major sources of decoherence in these devices was due to the use of lossy dielectrics -- initial devices used \chem{SiN_x} while state-of-the-art used hydrogenated amorphous silicon (a-Si:H) -- as well as two-level fluctuators which naturally existed in the \chem{AlO_x} barrier of the JJs used in these devices -- the overall size of the JJ was reduced here to in turn reduce the number of these fluctuators and the qubit frequency was then tuned away from the remaining fluctuators to obtain optimal performance. However, even with all of these updates to design and fabrication considerations, the state-of-the-art phase qubit coherence times remained $\sim1$~$\mu$s~\cite{Martinis:2008aa}, making future technologies with longer coherence times more attractive from a computational depth potential point of view.

\subsection{Flux qubit}

The persistent-current flux qubit was first examined in the late 1990s~\cite{Mooij:1999aa,Orlando:1999aa} with superpostiion observations~\cite{Wal:2000aa} and coherent operations~\cite{Chiorescu:2003aa} first taking place in devices tested in the early 2000s. Here, circuits are designed such that the quantum variable of interest is the direction of flux (current) through (around) a three-JJ SQUID loop\footnote{One of the reasons that three JJs are in use here instead of one is to add more inductance to the loop while also keeping the loop size small~\cite{Wendin:2017aa}.}, a sample schematic of which is drawn in Fig.~\refpart{fig:jjqb}{e}.

One of the biggest drawbacks of the traditional flux qubit is its sensitivity noise in this channel of control, and in particular the ubiquitous $1/f$ flux noise which has been observed in superconducting devices without full explanation since the 1980s\footnote{It is worth note here that recent work has shown a potential explanation of this noise in surface adsorbed \chem{O_2} onto the superconducting circuits under test~\cite{Kumar:2016aa}.}~\cite{Wellstood:1987aa,Weissman:1988aa}. However, in recent years progress has been realized in using a capacitively-shunted flux qubit~\cite{Yan:2016aa} with a modern fabrication process (reducing additional lossy dielectrics and using high quality materials) resulting in coherence times around 50~$\mu$s. While this is a major improvement from early devices, these qubits are still dominated by flux noise as a source of decoherence~\cite{Yan:2016aa}.

\subsection{Transmon\label{sec:jjtrans}}

The transmon qubit, or the \textit{transmission-line shunted plasma oscillation qubit}, is in principle designed to function similarly to the previously described CPB, but with exponentially lower sensitivity to charge noise on the drive line and is also designed to be embedded into a microwave environment (planar or 3D) which is engineered such that noise at the qubit transition frequency $\omega_{10}$ is suppressed~\cite{Koch:2007aa}. Both of these effects, coupled with other advances in materials research and fabrication advances, have had a dramatic effect on qubit coherence in the transmon regime where the relationship between $E_J$ and $E_C$ is typically $E_J/E_C\sim50$, meaning that the JJ coupling energy dominates the dynamics of the system (and thus $\hat{\delta}$ is a good quantum variable while $\hat{n}$ has minimal effect). One can take the Hamiltonian from Eq.~(\ref{eq:jjham}) and assume small $\delta$ to expand the cosine term to fourth order
\begin{align}
\hat{\mathcal{H}}&=4E_C\hat{n}^2-E_J\cos\hat{\delta}\approx\underbrace{4E_C\hat{n}^2+\frac{E_J}{2}\hat{\delta}^2}_{\textrm{HO}}-\frac{E_J}{4!}\hat{\delta}^4,\label{eq:htransmon}
\end{align}
where the harmonic oscillator (HO) term is perturbed by the quartic term of the cosine expansion. Using perturbation theory, one extracts energy transition frequencies 
\begin{align}
E_{m+1}-E_m&=\hbar\omega_{m+1,m}\simeq\sqrt{8E_JE_C}-E_C(m+1),\label{eq:transenergy}
\end{align}
giving the lowest two energy splittings as
\begin{align}
\hbar\omega_{10}&\simeq\sqrt{8E_JE_C}-E_C,\\
\hbar\omega_{21}&\simeq\sqrt{8E_JE_C}-2E_C,
\end{align}
showing that the anharmonicity of these two transitions is
\begin{align}
\alpha&=\omega_{21}-\omega_{10}\simeq -E_C/\hbar,\label{eq:transanharm}
\end{align}
making it a parameter which is tunable by changing the geometric capacitance shunting the JJ in this circuit~\cite{Koch:2007aa}. Typical device parameters for modern transmon qubits include fundamental transition frequencies near $\omega_{10}\sim5$~GHz, anharmonicities around $\alpha/2\pi\sim 200-400$~MHz, and exhibit coherence times approaching 100~$\mu$s\footnote{As is outlined in much of the cited material on this topic, a cause for the explosion in coherence is the use of fabrication processes with careful attention to the use of lossy dielectrics; they are either minimized or completely absent in modern devices. A sample processing flow will be described in detail in Chapter~\ref{chap:sfq}. However, it is worth noting that even with modern fabrication processes, qubit-environment coupling remains a dynamic issue which requires careful attention and calibration in larger-scale systems~\cite{Klimov:2018aa}.} enabling the construction and operation of modestly complex circuits~\cite{Gambetta:2017aa,Wendin:2017aa,Liu:2018aa}. 




\section{Superconducting Electronics}\label{sec:sce}

The field of superconducting classical electronics (SCE) relies heavily on JJs as their primary active element\footnote{The transistor is to CMOS as the JJ is to SCE.}. SCE as a field of study has a long history dating back to the late 1960s with the first demonstration of a {flip-flop} circuit employing a JJ~\cite{Matisoo:1967aa} inspiring visions and devices for potential large-scale integration (LSI) showing up as early as 1980~\cite{Anacker:1980aa}. An explosion of activity kicked off in the early 1990s after the seminal work describing a technology family which addressed the shortcomings of prior SCE devices and increased clock speeds to the order of 100~GHz using relatively simple building blocks~\cite{Mukhanov:1987aa,Kaplunenko:1989aa,Likharev:1991aa}. Since then, the field has spawned new technologies for superconducting logic circuits, all of which rely on JJs as their primary building blocks.

In this Section, we aim to provide a topical introduction to SCE devices based on JJs without diving into all possible details. While in Section~\ref{sec:scjj}, we primarily treated the JJ for its quantum mechanical properties, we will now utilize the JJ in circuits which are ultimately treated as classical logic gates. Here, we will provide detail where it is relevant for the context of this thesis and more broadly overview the more advanced topics of SCE to whet the reader's appetite for further delving into this massively-researched field.

\subsection{Superconducting Flux Quanta as Classical Bits}

\begin{figure}[t]
\includegraphics[width=\textwidth]{jjdevices/sfq.pdf}
\caption[Introductory SFQ circuit details]{\label{fig:sfqbase}
\textbf{(a)} Depiction of a typical SFQ flux pulse wherein the area under the curve is quantized to $\Phi_0=2.068\;\textrm{mV}\times\textrm{ps}=2.068\;\textrm{mA}\times\textrm{pH}$. The width of this pulse is given by the $L/R$ time constant of the JJ/resistor circuit which for typical parameters is on the order of $\sim1\;\textrm{ps}$.
\textbf{(b)} Sample Josephson transmission line (JTL) circuit at a point in time wherein the first and third JTL cell contain a flux quantum, making the ``binary'' representation of this circuit ``101.'' Each clock cycle of this circuit (the action during the next cycle is drawn with a green dashed-line) will cause the binary code to advance one cell to the right; assuming no incident pulse from the lab, the new binary code will be ``010.'' (dashed-line box) The $i$th JTL unit cell, each of which comprises an overdamped JJ (shunt elements not drawn) with critical current $I_{0,i}$, inductor $L_i$, and bias source $I_{b,i}$. Here, $I_{b,i}\approx0.7I_{0,i}$ and $L_iI_{0,i}\sim\Phi_0/2$ (see main text). 
\textbf{(c)} Voltage traces for the $V_i$ nodes identified in the previous panel. The circuit is temporally binned with $t_0$ representing the initial state of the circuit; the trace in each time bin represents the classical state of the circuit during that clock cycle, where the clock frequency here is $\omega_{\textrm{clk}}=2\pi/(t_{n+1}-t_n)$.
}
\end{figure}

The basic act of data transfer in typical superconducting logic circuits is accomplished via the shuttling around of flux pulses which propagate in time from point to point while maintaining a time integral governed by the second Josephson relation Eq.~(\ref{eq:jjv})
\begin{align}
\int_{\substack{\textrm{one}\\\textrm{pulse}}} V(t)dt&=\frac{\Phi_0}{2\pi}\int_{\delta_0}^{\delta_0+2\pi}\frac{d\delta}{dt}dt=\Phi_0=2.068\;\textrm{mV}\times\textrm{ps}=2.068\;\textrm{mA}\times\textrm{pH}, 
\end{align}
the superconducting flux quantum (SFQ). A sample curve of such a pulse is drawn in Fig~\refpart{fig:sfqbase}{a}. Shuttling bits from one part of a circuit to another in so-called rapid SFQ (RSFQ) circuits is typically done either with superconducting passive transmission line (PTL) -- typically a microstrip geometry~\cite{Andratsky:1990aa,Polonsky:1993aa} -- or with a Josephson transmission line (JTL) comprising serial inductors and parallel JJs through which SFQ pulses propagate [depicted in Figs.~\refpart{fig:sfqbase}{b},\refpart{fig:sfqbase}{c}]. Typical parameters of the JTL include biasing individual JJs near their critical current $I_{b,i}\simeq0.7I_{0,i}$ and constraining the cell inductance by $L_iI_{0,i}\simeq\Phi_0/2$~\cite{Kaplunenko:1989aa,Likharev:1991aa}.

Using these inductor/JJ combinations as building blocks, one can establish a complete classical logic gate set -- following deMorgan's theorem -- by showing that both \textsc{not} and either \textsc{and} or \textsc{or} operations can be defined. This was first shown in RSFQ circuits in the early 1990s~\cite{Kwong:1993aa,Polonsky:1993ab}, paving the way for the multitude of circuit design and research that has taken place since.

\subsection{Modern SCE}

Since the early 1990s with the advent of RSFQ technology~\cite{Likharev:1991aa} which relied heavily on resistor networks for distributed bias currents throughout the circuit chip, other technologies have been devised with different advantages and drawbacks. Here, we will briefly outline a select few of them and provide an overview as to the differences between the variants. For the advanced reader, the author recommends a more detailed overview of these technologies plus others that will not be covered here~\cite{Soloviev:2017aa}. 

\subsubsection*{Energy-efficient RSFQ}

The primary difference between traditional RSFQ and ERSFQ is the bias network. Whereas in traditional RSFQ resistor networks are used to distribute bias current, in ERSFQ variants these networks are replaced by combinations of JJs and inductors. This leads to a background power dissipation which is substantially lower than RSFQ~\cite{Kirichenko:2011aa}. Due to all of the extra JJ dynamics in the circuit from the bias JJ network, special care must be taken when evolving the phase of the logic elements since there is now phase dynamics in the bias to account for which was not present when using simple resistors~\cite{Soloviev:2017aa}. For the most part, however, the circuit is operated in a very similar fashion to RSFQ circuit counterparts with the difference that there are approximately $30-40\%$ more JJs required for proper operation~\cite{Mukhanov:2011aa}. Some of the earlier devices tested using this new scheme were shown to correctly operate up to $67$~GHz.

\subsubsection*{Reciprocal Quantum Logic}

A variant of superconducting logic circuit which replaces dc bias sources with an ac bias (similar to the quantum flux parametron technology~\cite{Harada:1987aa,Hosoya:1991aa,Inoue:2013aa,Takeuchi:2013aa}that also acts as the circuit clock is the reciprocal quantum logic (RQL) circuit family~\cite{Herr:2011aa}. Here, devices are comprised of JJ/inductor networks for both biasing and operation and data bits are transmitted as fluxon-antifluxon pairs (i.e., instead of a single positive fluxon constituting a logical 1 as in Fig.~\refpart{fig:sfqbase}{a}, here a logical 1 is a positive-negative pulse pair). Tested circuits are shown to operate correctly and with very low bit-error-rates up to $12$~GHz. 


\section{Summary}\label{sec:hybrid}

In this Chapter, we have briefly described several technologies which are powered by JJs in various configurations. Ever since Josephson's prediction about JJ dynamics in 1962, a vast wealth of research has been done in using these non-dissipative, non-linear devices in a variety of superconducting circuits. In the late 1980s, a major leap forward was made with the invention of RSFQ circuits which are able to compute and transmit data at speeds which are at least a factor of 20 faster than typical modern computer processors today. We briefly described this technology and provided several avenues of further reading for the interested reader. 

Along with the classical logic circuitry enabled by JJs, over the past 20 years, the field of superconducting quantum information processing (sQIP) has benefited greatly from the careful use of JJs as nonlinear inductors in slightly anharmonic oscillator potentials wherein the bottom two energy levels serve as a quantum computational manifold. Here, JJs are embedded in circuits which are carefully engineered to protect the coherence of the quantum state, taking into the consideration not only the coupling to the outside world but also the materials and fabrication required to create such devices. We have laid out a short introduction to some styles of superconducting qubits that have been studied and described some of their properties.

These two technologies -- superconducting classical and quantum circuits -- each are topics of intense research today, and this thesis represents one work which requires the use of both. While in this Chapter we have introduced basic elements of the two in a separate fashion, Chapter~\ref{chap:sfq} will be an overview of a successful combination of the two technologies into one operable device in which the classical SFQ circuit is used as a controller for the quantum transmon qubit. This work represents a major step towards the tight integration of the two technologies, the potential benefits of which are outlined with the presented results.