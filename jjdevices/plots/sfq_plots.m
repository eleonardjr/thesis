pulse = @(t,t0) exp(-(t-t0).^2);

figure; hold on;
t0 = 0;
t = -2.5:0.01:2.5;
plot(t,pulse(t,t0))
grid off
set(gca,'XMinorTick','off')
set(gca,'XTickLabel',[])
set(gca,'YTickLabel',[])
set(gca,'YMinorTick','off')


    
td = 10;
tf = 4*td;
t=0:0.1:tf;
figure; hold all;
subplot(3,1,1)
plot(t,pulse(t,td),'Color',[0    0.4470    0.7410])
grid off
set(gca,'XTickLabel',[])
set(gca,'YTickLabel',[])
subplot(3,1,2)
set(gca,'ColorOrderIndex',2)
plot(t,pulse(t,2*td),'Color',[0.8500    0.3250    0.0980])
grid off
set(gca,'XTickLabel',[])
set(gca,'YTickLabel',[])
subplot(3,1,3)
set(gca,'ColorOrderIndex',3)
plot(t,pulse(t,td)+pulse(t,3*td),'Color',[0.9290    0.6940    0.1250])
grid off
set(gca,'XTickLabel',[])
set(gca,'YTickLabel',[])