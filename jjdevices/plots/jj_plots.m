U = @(d,i) cos(d) - d*i;

x=0:0.01:6*pi;
figure; hold on;
plot(x,U(x,0),'DisplayName','i=0')
plot(x,U(x,0.5),'DisplayName','i=0.5')
plot(x,U(x,1),'DisplayName','i=1')

%axis tight
xlim([0 6*pi+0.1])
ylim([-20 1.1])
grid off
legend('show','Location','best')

xlabel('\delta')
ylabel('U_J (E_J)')

xticks([0 2*pi 4*pi 6*pi])
xticklabels({'0','2\pi','4\pi','6\pi'})
set(gca,'FontSize',12)

d=load('bha1939axk_LL 4K JJ4.hdf5.mat'); % sample unshunted JJ data
figure; hold on;
subplot(1,2,1)
plot(d.current*1000000,d.voltage*1000)
xlim([0 100])
ylim([-0.2 4])
set(gca,'FontSize',12)
grid off

d2 = load('shunted_slug_IV.mat');
%figure; hold on;
subplot(1,2,2)
plot(d2.current, d2.voltage/1000)
xlim([-5 105])
ylim([-10/1000 250/1000])
set(gca,'FontSize',12)
grid off

figure; hold on;
x=0:0.01:3*pi;
figure; hold on;
plot(x,U(x,0.2),'k-','DisplayName','i=0.2')
grid off
yticks([])
set(gca,'XMinorTick','off')